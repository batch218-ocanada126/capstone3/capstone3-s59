import ProductCatalog from '../components/ProductCatalog';
//import coursesData from '../data/coursesData';
import { useState, useEffect, useContext } from 'react';



export default function Product() {

	// state that will be used to store the courses retrieved from the database
	const [products, setProducts] = useState([])


	
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/activeProduct`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product =>{
				return(
				<ProductCatalog key={products._id} product={product}/>
				)
			}))
		})
	},[])


	// Props Drilling - allows us to pass information from one component to another using "props"
	// Curly braces {} are used for props to signify that we are providing/passing information
	return(
		<>
		{products}
		</>
		)
}