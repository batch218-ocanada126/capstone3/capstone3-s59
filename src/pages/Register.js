//s52 discussion code

import { useState, useEffect,useContext } from 'react'; // S54 ACTIVITY

import {Navigate} from 'react-router-dom'; // S54 ACTIVITY
import {useNavigate} from 'react-router-dom'; // S55 ACTIVITY

import Swal from 'sweetalert2'; // S54 ACTIVITY

import UserContext from '../UserContext'; // S54 ACTIVITY

//import  UsePasswordValidation from '../components/UsePasswordValidation';

import { Form, Button } from 'react-bootstrap';

export default function Register() {

  

    const {user} = useContext(UserContext); 
   

    const navigate = useNavigate(); 

    // State hooks to store the values of the input fields
    const [username, setUserName] = useState(""); // S55 ACTIVITY
    //const [firstName, setFirstName] = useState(""); // S55 ACTIVITY
    //const [lastName, setLastName] = useState(""); // S55 ACTIVITY
   // const [address, setaddress] = useState(""); // S55 ACTIVITY
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState(""); // S55 ACTIVITY
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    // State to determine wether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    
   // const isValid = UsePasswordValidation(password1);


    // Function to simulate redirection via form submission
   function registerUser(e) {
          // prevents page redirection via form submission
          e.preventDefault()

        

          fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
              method: "POST",
              headers: {
                  'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                 username: username,
                  email: email,
                  password: password1,
                  mobileNo: mobileNo
              })
          })
          .then(res => res.json())
          .then(data => {
              console.log(data)

              if (data === true) {

                  Swal.fire({
                      title: "Duplicate Email Found",
                      icon: "error",
                      text: "Kindly provide another email to complete registration."
                  })
              } else {

                  fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                      method: "POST",
                      headers: {
                          'Content-Type': 'application/json'
                      },
                      body: JSON.stringify({
                          username: username,
                         // firstName: firstName,
                          //lastName: lastName,
                          email: email,
                          mobileNo: mobileNo,
                          password: password1


                      })
                  })
                  .then(res => res.json())
                  .then(data => {
                      console.log(data)

                      if(data === true) {
                      // Clear input fields
                      setUserName("") ;   
                     // setFirstName("");
                     // setLastName("");
                      setEmail("");
                      setMobileNo("");
                      setPassword1("");
                      setPassword2("");

                      Swal.fire({
                              title: "Registration Successful",
                              icon: "success",
                              text: "Welcome to Zuitt!"
                          })

                          navigate("/login");

                      } else {

                          Swal.fire({
                              title: "Something went wrong",
                              icon: "error",
                              text: "Please, try again."
                          })
                      }
                  })
              }
          })

      }
   

    // S55 ACTIVITY
    useEffect(() => {
        

            // Validation to enable the submit button when all fields are populated and both passwords match.
            //if((firstName !=='' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' &&username !=='') && (password1 === password2)){

            if((email !== '' && password1 !== '' && password2 !== '' &&username !=='') && (password1 === password2)){    
                
                setIsActive(true);
            } else {
                setIsActive(false);
            }

        }, [email, password1, password2, username])




    return (

        (user.id !== null) ? 
        <Navigate to ="/login" /> 
        : 
    <div id="bg-register" className="min-vh-100">  
     <div className="row"> 
        <Form className="container-fluid col-6 mt-5 mb-5 p-5 min-vh-60 text-center border" id="registerForm" onSubmit={(e) => registerUser(e)}>

            {/*S55 ACTIVITY*/}
            <div className="mt-2  container-fluid border  col-6 " id="textR">Please enter your registration details</div>
           {/* <Form.Group className="mb-3 mt-5" controlId="firstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control 
                type="text"
                value={firstName}
                onChange={(e) => {setFirstName(e.target.value)}}
                placeholder="Enter your First Name" 
                required
                />
          </Form.Group>*/}

          {/*S55 ACTIVITY*/}
          {/*<Form.Group className="mb-3" controlId="lastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control 
                type="text"
                value={lastName}
                onChange={(e) => {setLastName(e.target.value)}}
                placeholder="Enter your Last Name" />
          </Form.Group>*/}

            <Form.Group className="mb-3" controlId="Username">
              <Form.Label>Username</Form.Label>
              <Form.Control 
                  type="text"
                  value={username}
                  onChange={(e) => {setUserName(e.target.value)}}
                  placeholder="Enter your Username" />
            </Form.Group>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
                type="email"
                value={email}
                onChange={(e) => {setEmail(e.target.value)}}
                placeholder="Enter email" />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          {/*S55 ACTIVITY*/}
          <Form.Group className="mb-3" controlId="mobileNo">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control 
                type="text"
                value={mobileNo}
                onChange={(e) => {setMobileNo(e.target.value)}}
                placeholder="+6399 9999 999" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password" 
                value={password1}
                onChange={(e) => {setPassword1(e.target.value)}}
                placeholder="Enter Your Password" />
             {/*<Form.Text className="text-muted">
                Password must have at least 1 capital letter, 1 special character, and a minimum of 8 characters .
             </Form.Text>*/}
                
          </Form.Group>

          <Form.Group className="mb-3" controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control 
                type="password" 
                value={password2}
                onChange={(e) => {setPassword2(e.target.value)}}
                placeholder="Verify Your Password" />
          </Form.Group>
          { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                     Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                      Submit
                    </Button>
          }
         
        </Form> 
      </div>
   </div>   

   )

}
