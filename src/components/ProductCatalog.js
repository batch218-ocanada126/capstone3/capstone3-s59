import {useState, useEffect} from 'react';

import { Button , Card} from 'react-bootstrap';

import PropTypes from 'prop-types'

import {Link} from 'react-router-dom';

export default function ProductCatalog({product}) {
	
	const {title, description, price, color, size, _id} = product;

	//count - getter
	//setCount - setter
	//useState(0) - useState(initialGetterValue) 
	// const  [count, setCount] = useState(0);

	// const  [seats, setSeats] = useState(5);
	// //s51 activity
	// //console.log(useState(0));
	// const [isOpen, setIsOpen] = useState(true);


	// function that keeps track of the enrollees for a course
	function enroll() {
	// 	setCount(count + 1);
	// 	console.log('Enrollees: ' + count);
	// // S51 ACTIVITY
	// 	setSeats(seats - 1);
	// 	console.log('Seats: ' + seats)

		// if(seats === 1){
		// 	alert("No more seats.")
		// 	document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
		// }

	// S51 ACTIVITY END


	};

	// useEffect allows us to execute function if the value of seats state changes.
	// useEffect(() =>{
	// 	if(seats === 0){
	// 	setIsOpen(false)	
	// 	alert("No more seats.")
	// 	document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
	// 	}
	// 	// will run anytime if one of the value in the array of dpendencies changes.
	// }, [seats])





	// Checks to see if the data is succesfully passed.
	//console.log(props);
	// Every components receives information in a form/type of an object
	//console.log(typeof props);	

	return (
	<Card id="bg-cardProduct" className="min-vh-50 container-fluid">
		<div className="row">
	   		 <Card.Body className="col-6 mx-auto">
	   		     <Card.Title>{title}</Card.Title>
	   		     <Card.Subtitle>Description:</Card.Subtitle>
	   		     <Card.Text>{description}</Card.Text>
	   		     <Card.Subtitle>Price:</Card.Subtitle>
	   		     <Card.Text>PHP {price}</Card.Text>
	   		     <Button className="bg-primary" as={Link} to={`/products/${_id}`} >Details</Button>
	   		    {/* <Card.Text>Enrollees: {count}</Card.Text>
	   		     <Card.Text>Seats: {seats}</Card.Text>
	   		     <Button id={'btn-enroll-' + id} className="bg-primary" onClick={enroll}>Enroll</Button>*/}
	   		 </Card.Body>
	   	</div>	 
	</Card>
	)
}

// "propTypes" - are a good way of checking data type of information between components.
ProductCatalog.propTypes = {
	// "shape" method is used to check if prop object conforms  to a specific "shape"
	product: PropTypes.shape({
		// Defined properties and their expected types
		title: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
	

		price: PropTypes.number.isRequired
	})
}